#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class ventana_principal():
    def __init__(self):
        # Constructor
        self.constructor = Gtk.Builder()
        self.constructor.add_from_file("VentanaPrincipal.glade")

        # Ventana
        self.ventana = self.constructor.get_object("VentanaPrincipal")
        self.ventana.set_default_size(800, 600)
        self.ventana.set_title("Ejercicio 1")
        self.ventana.connect("destroy", Gtk.main_quit)

        # Entrada de texto
        self.entrada1 = self.constructor.get_object("Entrada1")
        self.entrada2 = self.constructor.get_object("Entrada2")

        # Numerico
        self.numerico = self.constructor.get_object("Numerico")
        
        # El evento se activa al presionar Enter
        self.entrada1.connect("activate", self.suma)
        self.entrada2.connect("activate", self.suma)

        # Botones
        self.boton_aceptar = self.constructor.get_object("BotonAceptar")
        self.boton_aceptar.connect("clicked", self.mostrar_resultado)

        self.boton_reset = self.constructor.get_object("BotonReset")
        self.boton_reset.connect("clicked", self.abrir_advertencia)

        self.ventana.show_all()

        self.text1 = self.entrada1.get_text()
        self.text2 = self.entrada2.get_text()
        
    def suma(self, btn = None):

        self.text1 = self.entrada1.get_text()
        self.text2 = self.entrada2.get_text()

        self.num1 = len(self.text1)
        self.num2 = len(self.text2)

        self.suma = self.num1 + self.num2
        self.numerico.set_text(str(self.suma))

        self.entrada2.grab_focus_without_selecting()

    def mostrar_resultado(self, btn = None):
        resultado = mensaje_info(self.text1, self.text2,
                    self.suma, self.entrada1, self.entrada2, self.numerico)

    def abrir_advertencia(self, btn = None):
        mensaje = mensaje_advertencia(self.entrada1, self.entrada2, self.numerico)
        

class mensaje_info():
    def __init__(self, text1, text2, suma, entrada1, entrada2, numerico):
        # Constructor
        self.constructor = Gtk.Builder()
        self.constructor.add_from_file("Mensaje1.glade")

        # Ventana
        self.mensaje = self.constructor.get_object("VentanaMensaje")
        self.mensaje.set_default_size(400, 400)
        self.mensaje.set_title("Mensaje")
        self.mensaje.show_all()
        
        # Variables desde otra clase
        self.cadena1 = text1
        self.cadena2 = text2
        self.resultado = str(suma)
        self.text1 = entrada1
        self.text2 = entrada2
        self.numerico = numerico

        # Label ingreso texto
        self.texto = self.constructor.get_object("TextoIngresado")
        self.texto_label = self.texto.get_text() 
        
        self.texto_label = "\n" + self.cadena1 +  " " + self.cadena2
        self.texto.set_text(self.texto_label) # Label muestra texto   

        # Label suma
        self.texto_resultado = self.constructor.get_object("Suma")
        self.texto_suma = self.texto.get_text() 
        
        self.texto_suma = "\n" + self.resultado
        self.texto_resultado.set_text(self.texto_suma) # Label muestra resultado

        # Botones
        self.boton_aceptar = self.constructor.get_object("Aceptar")
        self.boton_aceptar.connect("clicked", self.limpiar_datos)

        self.boton_cancelar = self.constructor.get_object("Cancelar")
        self.boton_cancelar.connect("clicked", self.cerrar)
    
    def limpiar_datos(self, btn = None):
        self.mensaje.destroy()
        mensaje_informativo = mensaje()
        self.text1.set_text("") 
        self.text2.set_text("") 

        ajustes = Gtk.Adjustment(0, 0, 0, 0)
        self.numerico.set_adjustment(ajustes)

    def cerrar(self, btn = None):
        self.mensaje.destroy()
        

class mensaje():
    def __init__(self):
        # Constructor
        self.constructor = Gtk.Builder()
        self.constructor.add_from_file("Mensaje1.glade")

        # Ventana
        self.mensaje = self.constructor.get_object("Mensaje")
        self.mensaje.set_default_size(400, 400)
        self.mensaje.set_title("Mensaje informativo")
        self.mensaje.show_all()


class mensaje_advertencia():
    def __init__(self, entrada1, entrada2, numerico):
        # Constructor
        self.constructor = Gtk.Builder()
        self.constructor.add_from_file("Mensaje1.glade")

        # Ventana
        self.advertencia = self.constructor.get_object("VentanaAdvertencia")
        self.advertencia.set_default_size(400, 400)
        self.advertencia.set_title("Advertencia")
        self.advertencia.show_all()

        # Botones
        self.boton_aceptar = self.constructor.get_object("Ok")
        self.boton_aceptar.connect("clicked", self.limpiar_datos)

        self.boton_cancelar = self.constructor.get_object("Anular")
        self.boton_cancelar.connect("clicked", self.cerrar)
        
        self.text1 = entrada1
        self.text2 = entrada2
        self.numerico = numerico

    def limpiar_datos(self, btn = None):
        self.text1.set_text("") 
        self.text2.set_text("") 

        ajustes = Gtk.Adjustment(0, 0, 0, 0)
        self.numerico.set_adjustment(ajustes)

        self.advertencia.destroy()

    def cerrar(self, btn = None):
        self.advertencia.destroy()


if __name__ == "__main__":
    suma = ventana_principal()
    Gtk.main()
