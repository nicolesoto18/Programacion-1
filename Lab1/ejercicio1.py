M = 1000
D = 500
C = 100
L = 50
X = 10
V = 5
I = 1

def romano_arabico (numero_romano):
     suma = 0
     for i in range (0, len(numero_romano)):
        if (numero_romano[i] == 'M'):
            num = M
        elif (numero_romano[i] == 'D'):
            num = D
        elif (numero_romano[i] == 'C'):
            num = C
        elif (numero_romano[i] == 'L'):
            num = L
        elif (numero_romano[i] == 'X'):
            num = X
        elif (numero_romano[i] == 'V'):
            num = V
        elif (numero_romano[i] == 'I'):
            num = I
        return num

if __name__ == "__main__":
    print ("Ingrese un numero romano: ")
    numero_romano = (input ())
    final = 0

    if len(numero_romano) == 1:
        num = romano_arabico(numero_romano.upper())
        print("Numero arabico es: ", num)

    else:
        for i in range (0, len(numero_romano)):
            num = romano_arabico(numero_romano[i].upper())
            if numero_romano[0] < numero_romano[i]:
                print(numero_romano[0])
                num -= final
                final = num 
            else:
                print(numero_romano[0])
                final += num
        print("Numero arabico es: ", final)
    
