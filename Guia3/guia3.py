import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
import time


# NOTA: TODOS LOS EVENTOS SE ACTIVAN CON UN ENTER

# Algoritmo del modulo 11 utilizado para verificar el digito verificador
def digito_verificador(rut):
    value = 11 - sum([ int(a)*int(b)  for a,b in zip(str(rut).zfill(8), '32765432')])%11
    return {10: 'K', 11: '0'}.get(value, str(value))

# Funcion para verificar el formato del rut
def evaluar(rut):
    num = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'K', '.', '-']

    # Metodo upper() convierte cadenas en mayusculas
    for i in rut.upper():
        if i in num:
            # Metodo find() se utiliza para encontrar elementos
            if rut.find('.') != -1 and rut.find('-') != -1:
                # Metodo split() se utiliza para dividir cadenas, retorna un arreglo
                nuevo_rut = rut.split('.')
                resto = nuevo_rut[2].split('-')
                rut_final = nuevo_rut[0] + nuevo_rut[1] + resto[0] + resto[1]
                verificador = resto[1].upper()

                digito = digito_verificador(rut_final)

                if verificador == digito:
                    print("Rut correcto")
                    break 

                else:
                    texto = "\n\nRun incorrecto"
                    advertencia = ventana_advertencia(texto)
                    break
            else:
                texto = "\n\nFormato incorrecto"
                advertencia = ventana_advertencia(texto)
                break
        else:
            texto = "\n\nRun inválido"
            advertencia = ventana_advertencia(texto)
            break

# Funcion para revisar el formato del correo
def evaluar_correo(correo):
    signos = ['.', '_', '-']
    numeros = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']
    minusculas = ['a', 'b', 'c', 'd', 'e','f', 'g', 'h', 'i', 
                'j', 'k', 'l', 'm', 'n', 'o', 'p', 'k', 'r', 's',
                't', 'u', 'v', 'w', 'x', 'y', 'z']
    mayusculas = []
    dominios = ['gmail', 'hotmail', 'msn', 'yahoo', 'outlook', 'live']
    extensiones = ['com', 'net', 'com.mx', 'es', 'mx', 'org', 'gob']
    error = 0

    for mayuscula in minusculas:
        mayusculas.append(mayuscula.upper())

    # Si la cadena no es encontrada se devuelve -1
    if correo.find('@') != -1:
        nuevo_email = correo.split('@')
        usuario = nuevo_email[0]
        resto = nuevo_email[1]
        continuacion = resto.split('.')
        dominio = continuacion[0]
        terminacion = continuacion[1]
    
        for i in usuario:
            if i in signos or i in numeros or i in minusculas or i in mayusculas:
                if dominio in dominios:
                    if terminacion in extensiones:
                        print("Correo correcto")
                        break
                    else:
                        error = 1
                else:
                    error = 1
            else:
                error = 1
    else:
        error = 1

    if error == 1:
        texto = "\n\n El correo es invalido"
        advertencia = ventana_advertencia(texto)


def definir_mes():
    meses = {'ENE': 0,
            'FEB': 1,
            'MAR': 2,
            'ABR': 3,
            'MAY': 4,
            'JUN': 5,
            'JUL': 6,
            'AGO': 7,
            'SEP': 8,
            'OCT': 9,
            'NOV': 10,
            'DIC': 11}

    for i in meses:
        if i == time.strftime('%b').upper():
            return meses[i]


class ventana_principal():
    def __init__(self):
        self.constructor = Gtk.Builder()
        self.constructor.add_from_file("Ventanas.glade")

        self.ventana = self.constructor.get_object("VentanaPrincipal")
        self.ventana.set_default_size(500, 300)
        self.ventana.set_title("Ejercicio 3")
        self.ventana.connect("destroy", Gtk.main_quit)

        self.entrada_rut = self.constructor.get_object("EntradaRut")
        self.entrada_nombre = self.constructor.get_object("EntradaNombre")
        self.entrada_direccion = self.constructor.get_object("EntradaDireccion")
        self.entrada_correo = self.constructor.get_object("EntradaCorreo")
        self.entrada_diferencia = self.constructor.get_object("Diferencia")
        self.calendario = self.constructor.get_object("Calendario")

        self.entrada_rut.connect("activate", self.verificar_rut)
        self.entrada_nombre.connect("activate", self.verificar_nombre)
        self.entrada_correo.connect("activate", self.verificar_correo)
        self.entrada_direccion.connect("activate", self.verificar_nombre)
        
        # Se utiliza la fecha entregada desde el sistema para cambiar la del calendario
        self.dia = int(time.strftime('%d'))
        self.mes = definir_mes()
        self.anio = int(str(20) + time.strftime('%y'))
        self.calendario.select_day(self.dia)
        self.calendario.select_month(self.mes, self.anio) 

        # La fecha cambia si y solo si seleccionas una distinta
        self.calendario.connect("day-selected", self.valor_calendario)

        self.boton_info = self.constructor.get_object("BotonInfo")
        self.boton_info.connect("clicked", self.mostrar)

        self.nombre = self.entrada_nombre.get_text()
        self.direccion = self.entrada_direccion.get_text()
        self.rut = self.entrada_rut.get_text()
        self.correo = self.entrada_correo.get_text()
        self.fecha = self.calendario.get_date()

        self.ventana.show_all()
        self.entrada_direccion.hide()

    def verificar_nombre(self, evento):
        self.nombre = self.entrada_nombre.get_text()
        if len(self.nombre) != 0:
            # self.entrada_direccion.set_visibility(True)
            self.direccion = self.entrada_direccion.get_text()
            # La entrada de la direccion se vuelve a visualizar si el nombre es completado
            self.entrada_direccion.get_activates_default() 
            self.entrada_direccion.show_all()
            self.entrada_direccion.grab_focus_without_selecting()

        if len(self.nombre) == 0:
        # Si no se ha ingresado el nombre se esconde la entrada de la direccion 
            self.entrada_direccion.hide()
            self.entrada_nombre.grab_focus_without_selecting()

    def verificar_rut(self, evento):
        self.rut = self.entrada_rut.get_text()
        evaluar(self.rut)
        self.entrada_correo.grab_focus_without_selecting()
        
    def verificar_correo(self, evento):
        self.correo = self.entrada_correo.get_text()
        evaluar_correo(self.correo)

    def valor_calendario(self, event):
        # Se obtiene la fecha seleccionado
        self.fecha = self.calendario.get_date()
        dia = self.fecha[2]
        mes = self.fecha[1]
        anio = self.fecha[0]

        if dia > self.dia:
            resta_dia = dia - self.dia
        else:
            resta_dia = self.dia - dia

        resta_mes = self.mes - mes
        resta_anio = self.anio - anio
       
        resta = str(resta_dia) + " dias " + str(resta_mes) + " meses " + str(resta_anio) + " años " 
        self.entrada_diferencia.set_text(resta) 

    def mostrar(self, btn = None):
        if len(self.nombre) != 0 and len(self.direccion) != 0 and len(self.rut) != 0 and len(self.correo) != 0:
            datos = ventana_registro(self.nombre, self.direccion, self.fecha, self.rut, self.correo)
        else:
            texto = "\n\nNo ha completado los datos"
            advertencia = ventana_advertencia(texto)

class ventana_registro():
    def __init__(self, nombre, direccion, fecha, run, correo):
        self.constructor = Gtk.Builder()
        self.constructor.add_from_file("Ventanas.glade")

        self.ventana = self.constructor.get_object("VentanaRegistro")
        self.ventana.set_default_size(400, 200)
        self.ventana.set_title("Ejercicio 3")

        self.boton = self.constructor.get_object("BotonOk")
        self.boton.connect("clicked", self.cerrar)
        self.label_nombre = self.constructor.get_object("Nombre")
        self.label_direccion = self.constructor.get_object("Direccion")
        self.label_run = self.constructor.get_object("Rut")
        self.label_correo = self.constructor.get_object("Correo")
        self.label_fecha = self.constructor.get_object("Fecha")

        self.nombre = nombre
        self.direccion = direccion
        self.run = run
        self.correo = correo
        self.fecha = fecha
        self.fecha_final = str(self.fecha[2]) + '/' + str(self.fecha[1]) + '/' + str(self.fecha[0])

        texto1 = self.label_nombre.set_text(self.nombre)
        texto2 = self.label_direccion.set_text(self.direccion)
        texto3 = self.label_run.set_text(self.run)
        texto4 = self.label_correo.set_text(self.correo)
        texto5 = self.label_fecha.set_text(self.fecha_final)
        
        self.ventana.show_all()

    def cerrar(self, btn = None):
        self.ventana.destroy()

class ventana_advertencia():
    def __init__(self, texto_cambio):
        self.constructor = Gtk.Builder()
        self.constructor.add_from_file("Ventanas.glade")

        self.ventana = self.constructor.get_object("VentanaAdvertencia")
        self.ventana.set_default_size(300, 200)
        self.ventana.set_title("Ejercicio 3")
        self.ventana.show_all()
        
        self.label = self.constructor.get_object("Label")
        self.texto = texto_cambio
        texto = self.label.get_text()
        texto = self.label.set_text(self.texto)

        self.boton = self.constructor.get_object("BotonAceptar")
        self.boton.connect("clicked", self.cerrar)

    def cerrar(self, btn = None):
        self.ventana.destroy()


if __name__ == "__main__":
    suma = ventana_principal()
    Gtk.main()
